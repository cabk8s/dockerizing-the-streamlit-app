#Firstly we define our base image where we want to build our file from
FROM python:3.7

#Create a working directory
WORKDIR /app

#Copy all the requirements into the new directory created
COPY requirements.txt ./requirements.txt

#Install all that is in the requirments.txt file
RUN pip install -r requiremts.txt

#Expose the port to be used to run the application
#This is the same port that our streamlit app was running on
EXPOSE 8501

#Copy our app from the current directory to the working area
COPY ./app

#Create an entry point to make our image executable
ENTRYPOINT ["streamlit", "run"]

CMD ["app.py"]

